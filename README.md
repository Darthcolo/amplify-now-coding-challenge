# Amplify-Now Coding Challenge

#### Pretty Colours // Amplify Coding Challenge 2021

##### Context

The acclaimed animated film studio, Pisney Dixar, are finding it difficult to keep track of their colour schemes and have come to you for a digital solution.

##### Requirements

- Both colour schemes and their _groups_ should be nameable. For example, the colour scheme group might be called "Niding Femo" and in it there could be "Deep Ocean", "Dentist" and "Coral" colour schemes.

- A colour scheme needs to have at least 4 colours (for some reason Pisney doesn't like black and white anymore)

- In addition to manual input, a random button should shuffle all colours in the scheme

- JSON import / export

- No 3rd party libraries are to be used (aside from React)

##### Suggestions

- Write in React

- Its fine to just use HTML's built in colour selector

- Its fine to just support HEX

# Project deployed and live
You can access the project here: https://vigilant-euler-87de21.netlify.app/

# Pretty Colours coding challenge - bugs and issues
- [ ] When changing the name of a group, group names on buttons do not update until a new render
- [ ] When importing a group, the group name button does not appear until a new render
- [ ] When creating a new group, the group name button does not appear until a new render
- [ ] When changing active group, the previous colour scheme should disappear
- [ ] When importing a new colour scheme, the group list of schemes does not update until a new render
- [ ] No way to delete scheme groups
- [ ] No way to delete colour schemes