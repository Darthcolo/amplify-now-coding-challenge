# Data structure
    - An array of group objects
    - An array of scheme objects in each group object
    - An array of colours in each scheme object
  
```
[{
    groupName: 'Group 1',
    schemes: [{
        schemeName: 'Scheme 1',
        colours: ['colour1', 'colour2', 'colour3', 'colour4', 'colour5']
    },
    {
        schemeName: 'Scheme 2',
        colours: ['colour1', 'colour2', 'colour3', 'colour4', 'colour5']
    },
    {
        schemeName: 'Scheme 3',
        colours: ['colour1', 'colour2', 'colour3', 'colour4', 'colour5']
    },
    ...
    ]
},
{
    ...
}]
```