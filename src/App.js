import Header from './components/UI/Header';
import Group from './components/Group/Group';

// TODO: get this from local storage
const dataArray = [{
  groupName: 'Niding Femo',
  schemes: [{
    schemeName: 'Deep Ocean',
    colours: ['#03045e', '#023e8a', '#0077b6', '#0096c7', '#00b4d8']
  },
  {
    schemeName: 'Dentist',
    colours: ['#ede0d4', '#e6ccb2', '#ddb892', '#b08968', '#7f5539']
  },
  {
    schemeName: 'Coral',
    colours: ['#ece4db', '#d8e2dc', '#fec5bb', '#fec89a', '#e8e8e4']
  }]
},
{
  groupName: 'Demo Group 2',
  schemes: [{
    schemeName: 'Demo Scheme2 1',
    colours: ['#fb8d3d', '#b2008e']
  },
  {
    schemeName: 'Demo Scheme2 2',
    colours: ['#1396d7', '#bb5376', '#ec7226', '#86399f', '#356e83']
  }]
}];

function App() {
  return (
    <main>
      <Header />
      <section>
        <Group dataArray={dataArray} />
      </section>
    </main>
  );
}

export default App;
