import { useContext, useState } from "react";
import ImportScheme from "../IO/ImportScheme";
import Context from "../storage/app-context";
import classes from "./Scheme.module.css";

const SchemeAdd = props => {
    const { groupToShow, setDataArrayState, setUpdateFlag } = useContext(Context);
    const [newSchemeName, setNewSchemeName] = useState('');

    const schemeNameHandler = (e) => {
        setNewSchemeName(e.target.value);
    };

    const addNewSchemeHandler = (e) => {
        e.preventDefault();
        // Add new scheme to dataArray
        setDataArrayState(prevArray => {
            const activeGroupIndex = prevArray.findIndex(group => group.groupName === groupToShow);
            const newArray = prevArray;
            newArray[activeGroupIndex].schemes.push({ schemeName: newSchemeName, colours: [] });
            return newArray;
        });
        // Switch focus to the recently added scheme
        // TODO setSchemeToShow(newSchemeName);
        // Refresh the app to reflect the change
        setUpdateFlag(prevValue => !prevValue);
        // Empty input element
        setNewSchemeName('');
    };

    return (
        <>
            {!groupToShow
                ? <p>Please, first select a group, and then create or import a colour scheme</p>
                : <div className={classes['add-scheme-container']}>
                    <form onSubmit={addNewSchemeHandler}>
                        <input type="text" placeholder="Scheme name" onChange={schemeNameHandler} value={newSchemeName} className={classes['add-input-text']} required />
                        <input type="submit" value="Add new Scheme" className={classes['add-input-submit']} />
                    </form>
                    <span className={classes['add-divider']}></span>
                    <ImportScheme />
                </div>}

        </>
    );
};

export default SchemeAdd;