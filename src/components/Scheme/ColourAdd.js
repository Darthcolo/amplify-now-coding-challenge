import classes from './Scheme.module.css';

const ColourAdd = props => {

    const inputHandler = e => {
        props.addNewColour(e.target.value);
    };

    return (
        <div onKeyDown={inputHandler} className={classes['individual-colour__add']}>
            <label htmlFor="colour-input">Add a colour</label>
            <input id="colour-input" type="color" onBlur={inputHandler} />
        </div>
    );
};

export default ColourAdd;