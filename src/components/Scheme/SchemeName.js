import { useState, useEffect, useContext } from 'react';
import Context from '../storage/app-context';
import classes from './Scheme.module.css';

const SchemeName = props => {
    const [isSchemeNameChange, setIsSchemeNameChange] = useState(false);
    const { schemeToShow, setSchemeToShow, groupToShow, setDataArrayState } = useContext(Context);
    const [modifiedSchemeName, setModifiedSchemeName] = useState(schemeToShow);

    const changeSchemeNameHandler = () => {
        setIsSchemeNameChange(true);
    };

    const schemeNameHandler = (e) => {
        setModifiedSchemeName(e.target.value);
    };

    const schemeNameSubmitHandler = (e) => {
        e.preventDefault();
        setIsSchemeNameChange(false);
    };

    // Update dataArray and the current scheme
    useEffect(() => {
        setDataArrayState((prevArray => {
            const newArray = [...prevArray];
            // Find group index
            const groupIndex = newArray.findIndex(group => group.groupName === groupToShow);
            const schemeIndex = newArray[groupIndex].schemes.findIndex(scheme => scheme.schemeName === schemeToShow);
            newArray[groupIndex].schemes[schemeIndex].schemeName = modifiedSchemeName;
            return newArray;
        }));
        setSchemeToShow(modifiedSchemeName);
    }, [modifiedSchemeName]); // FIXME
    // }, [modifiedSchemeName, groupToShow, schemeToShow, setDataArrayState, setSchemeToShow]); // Fixed for deployment

    const schemeNameInput = <>
        <form onSubmit={schemeNameSubmitHandler}>
            <input type="text" value={modifiedSchemeName} onChange={schemeNameHandler} onBlur={schemeNameSubmitHandler} autoFocus />
            {/* <input type="submit" value="Set new name" /> */}
        </form>
    </>;

    return (
        <>
            <h3 key={Math.random()} onClick={changeSchemeNameHandler} className={classes['name-highlight']}>{isSchemeNameChange ? schemeNameInput : modifiedSchemeName}</h3><span className={classes['name-change']}>Click on name to change it</span>
        </>
    );
};

export default SchemeName;