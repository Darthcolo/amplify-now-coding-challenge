import classes from './Scheme.module.css';

const AlertMessage = props => {
    return (
        <p className={classes.alert}>Invalid colour! White (#FFFFFF) and black (#000000) are not allowed. Duplicated colours are not allowed.</p>
    );
};

export default AlertMessage;