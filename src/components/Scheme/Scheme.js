import { useState, useEffect } from 'react';
import AlertMessage from './AlertMessage';
import ColourAdd from "./ColourAdd";
import SchemeName from './SchemeName';
import classes from './Scheme.module.css';
import ExportScheme from '../IO/ExportScheme';

const COLOUR_LIMIT = 5;

const checkIfColourIsValid = (coloursArray, colour) => {
    if (colour === '#000000' || colour === '#ffffff' || coloursArray.includes(colour)) {
        return false;
    }
    return true;
};

const Scheme = props => {
    const [coloursArray, setColoursArray] = useState(props.schemeObject.colours);
    const [modifiedSchemeName] = useState(props.schemeObject.schemeName);
    const [isValid, setIsValid] = useState(true);
    const [isLimitReached, setIsLimitReached] = useState(false);


    // SECTION Add and remove colours

    const addNewColourHandler = (colour) => {
        if (!checkIfColourIsValid(coloursArray, colour)) {
            setIsValid(false);
            return;
        }

        if (isValid === false) {
            setIsValid(true);
        }

        setColoursArray((previousValue) => {
            props.schemeObject.colours = [...previousValue, colour];
            return [...previousValue, colour];
        });
    };

    const deleteColourHandler = e => {
        setColoursArray((previousValue) => {
            const filteredArray = previousValue.filter(colour => colour !== e.target.id);
            props.schemeObject.colours = [...filteredArray];
            return filteredArray;
        });
    };

    useEffect(() => {
        if (coloursArray.length > 4) {
            setIsLimitReached(true);
            return;
        }
        if (isLimitReached === true) {
            setIsLimitReached(false);
        }
    }, [coloursArray, isLimitReached]);

    // SECTION Random colours

    const randomColoursHandler = (e) => {
        for (let i = coloursArray.length; i < COLOUR_LIMIT; i++) {
            const randomHexNum = Math.round((Math.random() * 0xffffff)).toString(16);
            const hexColour = '#' + '0'.repeat(6 - randomHexNum.length) + randomHexNum;

            setColoursArray((previousValue) => {
                if (!checkIfColourIsValid(coloursArray, hexColour)) {
                    i--; // Annulate current step
                    return;
                }
                props.schemeObject.colours = [...previousValue, hexColour];
                return [...previousValue, hexColour];
            });
        }
    };

    // SECTION Colour buttons

    const schemeColours = coloursArray.map(colour => {
        return (
            <div key={colour} className={classes['individual-colour']} style={{ backgroundColor: colour, color: colour }}>
                <div className={classes['individual-colour__remove']}>{colour}
                    <button id={colour} onClick={deleteColourHandler}>X</button>
                </div>
            </div>
        );
    });

    // If the scheme name is modified due to a scheme import
    useEffect(() => {
        props.schemeObject.schemeName = modifiedSchemeName;
    }, [modifiedSchemeName, props.schemeObject]);

    return (
        <>
            <div className={classes['scheme-container']}>
                <SchemeName schemeName={modifiedSchemeName} />
                <div className={classes['colour-container']}>
                    {schemeColours}
                </div>
                {!isLimitReached && <ColourAdd addNewColour={addNewColourHandler} />}
                {!isValid && <AlertMessage />}
                {!isLimitReached && <button onClick={randomColoursHandler} className={classes['add-input-submit']}>Shuffle colours</button>}
                <ExportScheme fileName={modifiedSchemeName} exportData={props.schemeObject} />
            </div>
        </>
    );
};

export default Scheme;