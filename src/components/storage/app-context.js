import React from 'react';

// Context definition

const Context = React.createContext({});

export default Context;