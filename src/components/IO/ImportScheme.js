import { useState, useContext, useEffect } from 'react';
import Context from '../storage/app-context';
import classes from '../Scheme/Scheme.module.css';

const ImportScheme = props => {
    const { setDataArrayState, groupToShow } = useContext(Context);
    const [importedObject, setImportedObject] = useState({});

    const importHandler = (e) => {
        // If there are no files (file import cancelled)
        if (!e.target.files[0]) {
            return;
        }
        // Get the uploaded file (make sure is just one, and select the first one)
        const fileObj = e.target.files[0];
        // Read the content of the file
        const reader = new FileReader();
        // The 'onload' event handler is triggered when the read completes
        reader.onload = (e) => {
            const object = JSON.parse(e.target.result);
            // Insert new Scheme on active group
            setImportedObject(object);
        };
        // Read the file
        reader.readAsText(fileObj);
    };

    // Store the imported scheme in the main dataArray
    useEffect(() => {
        if (Object.keys(importedObject).length > 0) {
            setDataArrayState(prevArray => {
                const activeGroupIndex = prevArray.findIndex(group => group.groupName === groupToShow);
                const newArray = prevArray;
                newArray[activeGroupIndex].schemes.push(importedObject);
                return newArray;
            });
        }
    }, [importedObject]); // FIXME
    // }, [importedObject, groupToShow, setDataArrayState]); // Fixed for deployment

    return (
        <div key={Math.random()}>
            <label htmlFor="input-scheme-file" className={classes['io-button']}>Import colour scheme</label>
            <input
                id="input-scheme-file"
                type="file"
                multiple={false}
                accept=".json"
                onChange={importHandler}
                className={classes.hidden}
            />
        </div>
    );
};

export default ImportScheme;