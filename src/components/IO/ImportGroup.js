import { useContext } from 'react';
import Context from '../storage/app-context';
import classes from '../Scheme/Scheme.module.css';

const ImportGroup = () => {
    const { setDataArrayState } = useContext(Context);

    const importHandler = (e) => {
        // If there are no files (file import cancelled)
        if (!e.target.files[0]) {
            return;
        }
        // Get the uploaded file (make sure is just one, and select the first one)
        const fileObj = e.target.files[0];
        // Read the content of the file
        const reader = new FileReader();
        // The 'onload' event handler is triggered when the read completes
        reader.onload = (e) => {
            const object = JSON.parse(e.target.result);
            // Set group data
            setDataArrayState(prevData => {
                return [...prevData, object];
            });
        };
        // Read the file
        reader.readAsText(fileObj);
    };

    return (
        <div key={Math.random()}>
            <label htmlFor="input-group-file" className={classes['io-button']}>Import group</label>
            <input
                id="input-group-file"
                type="file"
                multiple={false}
                accept=".json"
                onChange={importHandler}
                className={classes.hidden}
            />
        </div>
    );
};

export default ImportGroup;