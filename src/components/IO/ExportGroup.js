import classes from '../Scheme/Scheme.module.css';

const ExportGroup = props => {
    const exportHandler = (e) => {
        // Transform the object into JSON format
        const output = JSON.stringify(props.exportData, null, 4);
        // Create a Blob object, a file-like object of immutable, raw data
        const blob = new Blob([output]);
        // The URL.createObjectURL() static method creates a DOMString containing a URL representing the object given in the parameter
        const fileDownloadUrl = URL.createObjectURL(blob);
        // Temporal download link
        let tempLink = document.createElement('a');
        tempLink.href = fileDownloadUrl;
        tempLink.setAttribute('download', `${props.fileName}.json`);
        tempLink.click();
        // Free up storage
        URL.revokeObjectURL(fileDownloadUrl);
    };

    return (
        <button onClick={exportHandler} className={classes['io-button']} style={{ margin: '.5rem auto' }}>Export group</button>
    );
};

export default ExportGroup;