import { useContext, useState } from "react";
import ImportGroup from "../IO/ImportGroup";
import Context from "../storage/app-context";
import classes from './Group.module.css';

const GroupAdd = props => {
    const { setDataArrayState, setUpdateFlag } = useContext(Context);
    const [newGroupName, setNewGroupName] = useState('');

    const groupNameHandler = (e) => {
        setNewGroupName(e.target.value);
    };

    const addNewGroupHandler = (e) => {
        e.preventDefault();
        // Add new group to dataArray
        setDataArrayState(prevArray => {
            const newArray = prevArray;
            newArray.push({ groupName: newGroupName, schemes: [] });
            return newArray;
        });
        // Refresh the app to reflect the change
        setUpdateFlag(prevValue => !prevValue);
        // Empty input element
        setNewGroupName('');
    };

    return (
        <>
            <div className={classes['add-group-container']}>
                <form onSubmit={addNewGroupHandler}>
                    <input type="text" placeholder="Group name" onChange={groupNameHandler} value={newGroupName} className={classes['add-input-text']} required />
                    <input type="submit" value="Add new Group" className={classes['add-input-submit']} />
                </form>
                <span className={classes['add-divider']}></span>
                <ImportGroup />
            </div>
        </>
    );
};

export default GroupAdd;