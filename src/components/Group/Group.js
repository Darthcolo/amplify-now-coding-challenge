import { useEffect, useState } from "react";
import Context from "../storage/app-context";
import Scheme from "../Scheme/Scheme";
import SchemeAdd from "../Scheme/SchemeAdd";
import ExportGroup from "../IO/ExportGroup";
import GroupAdd from "./GroupAdd";
import GroupName from "./GroupName";
import classes from "./Group.module.css";

// TODO Group names should be unique
// TODO Check imported data before using it

const Group = props => {
    const [groupToShow, setGroupToShow] = useState('');
    const [schemeToShow, setSchemeToShow] = useState('');
    const [schemeObject, setSchemeObject] = useState({});
    const [modifiedGroupName, setModifiedGroupName] = useState('');
    const [dataArrayState, setDataArrayState] = useState(props.dataArray);
    const [groups, setGroups] = useState('');
    const [updateFlag, setUpdateFlag] = useState(false);

    const [groupNames, setGroupNames] = useState(() => {
        const newArray = [];
        dataArrayState.forEach(group => newArray.push(group.groupName));
        return newArray;
    });
    const [groupButtons, setGroupButtons] = useState(() => {
        const groupButtons = groupNames.map(groupName => {
            return <button
                key={Math.random()}
                onClick={setGroupToShow(groupName)}
                // Prevent the user from clicking on the same scheme more than once
                disabled={groupToShow === groupName}
                className={classes['group-button']}>
                {groupName}
            </button>;
        });
        return groupButtons;
    });

    // Get the group and scheme name from the groupAndSchemeObject
    const callIndividualSchemeHandler = (groupAndSchemeObject) => () => {
        setGroupToShow(groupAndSchemeObject.myGroup);
        setSchemeToShow(groupAndSchemeObject.myScheme);
        // Trigger an update of the schemeObject
        setSchemeObject('');
    };

    // Create and update scheme object to be rendered
    useEffect(() => {
        if (schemeToShow !== '') {
            const filteredSchemeObject = dataArrayState.filter(group => {
                return group.groupName === groupToShow;
            })[0].schemes.filter(scheme => {
                return scheme.schemeName === schemeToShow;
            })[0];
            // If no matching Scheme, a Scheme from another Group was selected
            if (!filteredSchemeObject) {
                return;
            }
            setSchemeObject(filteredSchemeObject);
        }
    }, [schemeToShow, groupToShow, dataArrayState]);

    // Update group to display
    const updateGroup = (e) => {
        setGroupToShow(e.target.value);
        setUpdateFlag(prevValue => !prevValue);
    };

    // Display available group names
    useEffect(() => {
        // Set available group names
        setGroupNames(() => {
            const newArray = [];
            dataArrayState.forEach(group => newArray.push(group.groupName));
            return newArray;
        });

        // Show group buttons
        setGroupButtons(() => {
            const groupButtons = groupNames.map(groupName => {
                return <button
                    key={Math.random()}
                    onClick={updateGroup}
                    // Prevent the user from clicking on the same scheme more than once
                    disabled={groupToShow === groupName}
                    value={groupName}
                    className={classes['group-button']}>
                    {groupName}
                </button>;
            });
            return groupButtons;
        });
    }, [dataArrayState, updateFlag]); // FIXME
    // }, [dataArrayState, updateFlag, groupNames, groupToShow]); // Fixed for deployment


    // TODO Delete group
    // const deleteGroupHandler = (e) => {
    //     console.log('DELETE:', e.target.value);
    //     setDataArrayState((prevDataArray) => {
    //         const newArray = prevDataArray.filter(group => {
    //             return group.groupName !== e.target.value;
    //         });
    //         return newArray;
    //     });
    //     setUpdateFlag(prevValue => !prevValue);
    // };

    // Get all groups and scheme names
    useEffect(() => {
        const groups = dataArrayState.map(group => {
            // Display only the active group
            if (group.groupName === groupToShow) {
                // Get the group name
                const groupHeader = <GroupName groupNameIndex={groupNames.indexOf(group.groupName)} />;
                // Get all the corresponding schemes for the group
                const schemeButtons =
                    group.schemes.map(scheme => {
                        const groupAndSchemeObject = { myGroup: group.groupName, myScheme: scheme.schemeName };
                        return <button
                            key={Math.random()}
                            onClick={callIndividualSchemeHandler(groupAndSchemeObject)}
                            // Prevent the user from clicking on the same scheme more than once
                            disabled={schemeToShow === scheme.schemeName}>
                            {scheme.schemeName}
                        </button>;
                    });

                return (
                    <div key={Math.random()} className={classes['group-container']} >
                        {groupHeader}
                        {schemeButtons}
                        <hr style={{ width: '95%' }} />
                        <ExportGroup fileName={groupNames[groupNames.indexOf(group.groupName)]} exportData={dataArrayState[groupNames.indexOf(group.groupName)]} />
                        {/* TODO <button onClick={deleteGroupHandler} value={group.groupName}>Delete group</button> */}
                    </div >
                );
            } else {
                return null;
            }
        });
        setGroups(groups);
    }, [groupToShow, dataArrayState, groupNames, schemeToShow, updateFlag]);

    return (
        <Context.Provider value={{
            dataArrayState,
            setDataArrayState,
            modifiedGroupName,
            setModifiedGroupName,
            groupNames,
            setGroupNames,
            groupToShow,
            setGroupToShow,
            schemeToShow,
            setSchemeToShow,
            updateFlag,
            setUpdateFlag
        }}>
            <GroupAdd />
            <p className={classes.centred}>Select a group by clicking on the buttons below</p>
            <div className={classes['group-button-container']}>
                {groupButtons}
            </div>
            <div className={classes['groups-main-container']}>
                {groups}
            </div>
            <div className={classes['scheme-container']}>
                <SchemeAdd />
                {Object.keys(schemeObject).length > 0 && <Scheme schemeObject={schemeObject} />}
            </div >
        </Context.Provider>
    );
};

export default Group;