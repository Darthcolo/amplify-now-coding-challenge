import { useState, useContext } from 'react';
import Context from '../storage/app-context';
import classes from './Group.module.css';

const GroupName = props => {
    const [isGroupNameChange, setIsGroupNameChange] = useState(false);
    const { groupNames, setGroupNames, setDataArrayState, setGroupToShow } = useContext(Context);
    const [modifiedGroupName, setModifiedGroupName] = useState(groupNames[props.groupNameIndex]);

    const changeGroupNameHandler = (e) => {
        setIsGroupNameChange(true);
        setModifiedGroupName(e.target.textContent);
    };

    const groupNameHandler = (e) => {
        setModifiedGroupName(e.target.value);
    };

    const groupNameSubmitHandler = (e) => {
        e.preventDefault();
        // Update the main dataArray with new data (FIXME I'm using two arrays, I should just use the main dataArray)
        setGroupNames(prevArray => {
            return prevArray;
        });
        setDataArrayState((prevArray => {
            const newArray = [...prevArray];
            newArray[props.groupNameIndex].groupName = modifiedGroupName;
            return newArray;
        }));
        setGroupToShow(modifiedGroupName);

        setIsGroupNameChange(false);
    };

    const groupNameInput = <>
        <form onSubmit={groupNameSubmitHandler}>
            <input type="text" value={modifiedGroupName} onChange={groupNameHandler} onBlur={groupNameSubmitHandler} autoFocus />
            {/* <input type="submit" value="Set new name" /> */}
        </form>
    </>;

    return (
        <>
            <h2 onClick={changeGroupNameHandler} className={classes['name-highlight']}>
                {isGroupNameChange ? groupNameInput : groupNames[props.groupNameIndex]}
            </h2>
            <span className={classes['name-change']}>Click on name to change it</span>
        </>
    );
};

export default GroupName;