import classes from './Header.module.css';

const Header = props => {
    return (
        <header className={classes['header-container']}>
            <h1>Pretty Colours</h1>
            <p><i>a Pisney Dixar colour scheme tracker</i></p>
        </header>
    );
};

export default Header;